import rfoo
import sys
ROOT_PORT = 52142
from rfoo.utils import rconsole


### Reverse hook
import rfoo
connected_to_watchers = []

api=None
try:
    api = get_ipython()
except:
    import IPython
    api = IPython.ipapi.get()

def pre_prompt_hook(self):
    remove_list = []
    for watcher in connected_to_watchers:
        try:
            rfoo.Proxy(watcher).runsource("refresh_watches()")
        except:
            remove_list.append(watcher)
    for w in remove_list:
        connected_to_watchers.remove(w)
if api:
    api.set_hook("pre_prompt_hook", pre_prompt_hook)
def add_watcher(port):
    conn_to_watcher = rfoo.InetConnection().connect(port=port)
    connected_to_watchers.append(conn_to_watcher)

def run_pywatch_server(port=None):
    if port:
        rconsole.spawn_server(port=port)
    else:
        i=0
        server_ready = False
        while i<10 and not server_ready :
            try:
                rconsole.spawn_server(port=ROOT_PORT+i)
                server_ready = True
            finally:
                i += 1
        if not server_ready:
            raise ValueError("Cannot raise server")
        port = ROOT_PORT+i-1
    print "watcher server listening on port %s"%port
