#! /usr/bin/python
# doc string
import pickle
import curses
import time
import sys
import rfoo
import pdb
from rfoo.utils import rconsole

main_windows = []
attempts = 3
conn = None
while not conn:
    try:
        conn = rfoo.InetConnection().connect(port=int(sys.argv[1]))
    except:
        if attempts:
            time.sleep(1)
            attempts = attempts-1
        else:
            print "cannot open reverse rconsole"
            sys.exit(1)


KEY_COLOR = 1
VALUE_COLOR = 2
ERROR_COLOR = 3

ROOT_PORT=52173

apps = []

listen_port = int(sys.argv[2]) if len(sys.argv)>2 else ROOT_PORT


def refresh_watches():
    for app in apps:
        app.refresh_watches()

class PyWatch():
    def __init__(self, stdscr):
        self.stdscr = stdscr
        stdscr.keypad(1)
        curses.cbreak()
        self.listen_port = 0
        curses.init_pair(KEY_COLOR, 7, 4)
        curses.init_pair(VALUE_COLOR, 7, 3)
        curses.init_pair(ERROR_COLOR, 7, 1)
        self.input_window = InputWindow(self, curses.newwin(3, stdscr.getmaxyx()[1], stdscr.getmaxyx()[0]-2, 0))
        self.watch = WatchPanel(self, curses.newwin(stdscr.getmaxyx()[0]-2, stdscr.getmaxyx()[1], 0,0))
        self.set_hook_and_reverse_connect()
        self.input_window.set_status("port = %s"%self.listen_port)
        apps.append(self)
    def refresh_watches(self):
        self.watch.refresh_watches()
    def main_loop(self):
        self.watch.main_loop()
    def set_hook_and_reverse_connect(self):
        self.listen_port = listen_port
        rconsole.spawn_server(port=self.listen_port)
        time.sleep(0.5)
        cmd = "add_watcher(port=%s)"%self.listen_port
        rfoo.Proxy(conn).runsource(cmd)
        #cmd = "globals().setdefault('connected_to_watchers',[]).append(conn_to_watcher)"
        #rfoo.Proxy(conn).runsource(cmd)

    def get_input(self, question=""):
        return self.input_window.get_input(question)


class Watch(object):
    def __init__(self, key, y, x, max_value=10):
        self.key = key
        self.max_value = max_value
        self.x = x
        self.y = y
        self.value = ""
    def refresh_value(self):
        more, output = rfoo.Proxy(conn).runsource(self.key)
        self.value = (" ".join(output.splitlines())+" "*self.max_value)[:self.max_value]
    def addstr(self, window):
        window.addstr(" "+self.key+" ", curses.color_pair(KEY_COLOR))
        window.addstr(" "+self.value+" ", curses.color_pair(VALUE_COLOR))
    def paint(self, window):
        window.move(self.y, self.x)
        self.addstr(window)
    @property
    def size(self):
        return 4+self.max_value+len(self.key)

class WatchPanel(object):
    def __init__(self, app, window):
        self.app = app
        self.window = window
        self.y = window.getyx()[0]
        self.x = window.getyx()[1]
        self.maxy = window.getmaxyx()[0]
        self.maxx = window.getmaxyx()[1]
        self.watches = []
        self.filename = None
    def move_cursor(self):
        self.window.move(self.y, self.x)

    def main_loop(self):
        while True:
            c = self.window.getch()
            watch = self.on_watch()
            if c == ord('l'):
                self.x = min(self.x+1, self.maxx)
            if c == ord('L'):
                if watch and watch.x+watch.size<self.maxx:
                    watch.x+=1
                    self.x+=1
                self.window.clear()
            if c == ord('h'):
                self.x = max(self.x-1, 0)
            if c == ord('H'):
                if watch and watch.x!=0:
                    watch.x-=1
                    self.x-=1
                self.window.clear()
            if c == ord('j'):
                self.y = min(self.y+1, self.maxy-1)
            if c == ord('J'):
                if watch and watch.y+1 < self.maxy:
                    watch.y+=1
                    self.y+=1
                self.window.clear()
            if c == ord('k'):
                self.y = max(self.y-1, 0)
            if c == ord('K'):
                if watch and self.y!=0:
                    watch.y-=1
                    self.y-=1
                self.window.clear()
            if c == ord('a'):
                new_watch = Watch(self.get_text("new watch "), self.y, self.x)
                self.watches.append(new_watch)
                self.refresh_watches()
            if c == ord('+'):
                if watch:
                    watch.max_value += 1
            if c == ord('-'):
                if watch:
                    watch.max_value = max(0, watch.max_value-1)
                self.window.clear()
            if c == ord('d'):
                if watch:
                    self.watches.remove(watch)
                    self.window.clear()
            if c == ord('r'):
                if watch:
                    watch.key = self.get_text("change key ")
                self.window.clear()
                self.refresh_watches()
            if c == ord('S'):
                pickle.dump(self.watches, open(self.get_text("saveas "), "w"))
                self.window.clear()
            if c == ord('e'):
                self.watches = pickle.load(open(self.get_text("open ")))
                self.window.clear()
                self.refresh_watches()
            self.refresh_watches()
            self.window.refresh()
    def refresh_watches(self):
        for watch in self.watches:
            watch.refresh_value()
        self.repaint_watches()
        self.window.refresh()
    def repaint_watches(self):
        for watch in self.watches:
            watch.paint(self.window)
        self.move_cursor()

    def on_watch(self):
        res = None
        for watch in self.watches:
            if self.y==watch.y and self.x>=watch.x and self.x <= watch.x + watch.size:
                res = watch
        return res

    def get_text(self, question=""):
        res = self.app.get_input(question)
        self.window.refresh()
        return res

class InputWindow(object):
    def __init__(self, app, window):
        self.app = app
        self.window = window
        self.status = ""
        self.refresh()
    def create_border(self):
        self.window.refresh()
        self.window.move(0,0)
        self.window.addstr("-"*self.window.getmaxyx()[1])
    def get_input(self, question=""):
        self.window.clear()
        self.create_border()
        self.window.move(1,0)
        self.window.addstr(question)
        self.window.refresh()
        current = ""
        c = None
        while c!=10:
            c = self.window.getch()
            if c!=10:
                self.window.addstr(chr(c))
                current = current+chr(c)
                self.window.refresh()
        self.refresh()
        return current

    def refresh(self):
        self.window.clear()
        self.create_border()
        self.window.move(1,0)
        self.window.addstr(self.status)
        self.window.refresh()

    def set_status(self, new_status):
        self.status = new_status
        self.refresh()

def init_pywatch(stdscr):
    "pass to curses.wrapper"
    app = PyWatch(stdscr)
    app.main_loop()
if __name__ == "__main__":
    curses.wrapper(func=init_pywatch)
